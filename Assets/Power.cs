﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Power : MonoBehaviour
{
    public Slider slider;
    public Animator animator;
	public Text value;
	public Image Fill;

    
    private float m = 0;
    void Start(){}
    void Update () {
		if(/*Input.GetKey(KeyCode.Space)*/ Input.GetMouseButton(0)){
			if(m<=1) m = m + 0.01f;
			else m =1;
			Fill.color = Color.Lerp(Color.green, Color.red, m);
			slider.value = m;
			value.text = (int)(m*100) + "% POWER";
		}
		else if (Input.GetKeyDown(KeyCode.Escape)) Application.Quit();
		else if (/*Input.GetKeyUp(KeyCode.Space)*/Input.GetMouseButtonUp(0)){
			Debug.Log(m);
			if(m>0.05){
                animator.SetTrigger("Trigger");
                animator.SetFloat("Power",m);    
            }
			m=0;
			slider.value = m;
			value.text = (int)(m*100) + "% POWER";
			Fill.color = Color.green;
		}
	}
}
